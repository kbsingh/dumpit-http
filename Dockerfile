FROM centos:centos7
MAINTAINER Karanbir Singh <kbsingh@karan.org>

RUN yum -y install httpd php

RUN chmod 777 -R /run/httpd && \
    chmod 777 -R /etc/httpd/logs/ && \
    sed -i -e 's/Listen 80/Listen 8080/g' /etc/httpd/conf/httpd.conf

ADD root /

EXPOSE 8080
USER 997
ENTRYPOINT ["/run.sh"]
